# PerDev

#### 项目介绍
Per.js web app的Skyogo工作室官方Chrome内核浏览器调试插件。
![PerDev](https://images.gitee.com/uploads/images/2018/1011/103705_b9c31f4d_1687981.png "0a41a3db43e087679f9934dc35ac88d8fd8.png")

#### 版本说明
- 1.0版本对应的Per.js版本应为2.4
- 1.1版本对应的Per.js版本应为2.5
- 1.2版本对应的Per.js版本应为2.5+

#### 安装教程

1. 选一个Community文件夹里面的版本文件夹，然后整个都下载下来
2. 在Chrome或者任何chrome内核浏览器的地址栏中输入：chrome://extensions（或者手动打开chrome扩展程序页面）
3. 打开开发者模式
4. 加载已解压的扩展程序

#### 使用说明

1. 在需要测试的Per.js web app页面里面按下F12，打开开发者工具
2. 里面有一个菜单选项就是PerDev

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request