// 创建自定义面板，同一个插件可以创建多个自定义面板
// 几个参数依次为：panel标题、图标（其实设置了也没地方显示）、要加载的页面、加载成功后的回调
chrome.devtools.panels.create('PerDev', 'img/icon.png', '../index.html', function(panel){
    function sel(el){
        return document.querySelector(el);
    }
    var version = 1.2;
    //记得更改Per.js的版本号
    sel("#title h1").innerHTML += "<span style='color:gray;'>"+version+"</span><span style='font-size:16px;'>&nbsp;&nbsp;for Per.js >= 2.6<span>";
    chrome.devtools.inspectedWindow.eval("Per().isThisModuleUsed('Per.component')", function(result, isException){
        if(result == true){
            chrome.devtools.inspectedWindow.eval("Per().component.getAllComponent()", function(result, isException){
                for(var i=0;i<result.length;i++){
                    if(i%2 == 0){
                        sel("#all-components").innerHTML += "<div class='com-name'>"+result[i].replace(/\</g,"&lt;").replace(/\>/g,"&gt;")+":</div>";
                        sel("#all-components").innerHTML += "<div class='com-tem'>"+result[++i].replace(/\</g,"&lt;").replace(/\>/g,"&gt;")+"</div><br>";
                    }
                }
            });
        }else{
            sel("#real-window-component").innerHTML = "<h2 style='color:red;'>无法访问此页面，因为你没有加载Per.component包</h2>";
        }
    });
    chrome.devtools.inspectedWindow.eval("Per().version", function(result, isException){
        if(result <= 2.5){
            sel("#real-window").innerHTML = "<h2 style='color:red;'>本插件仅支持版本等于大于2.5的Per.js文件，请更新版本！</h2>";
        }
    });
    sel("#component-refresh").onclick = function(){
        sel("#all-components").innerHTML = "";
        chrome.devtools.inspectedWindow.eval("Per().component.getAllComponent()", function(result, isException){
            for(var i=0;i<result.length;i++){
                if(i%2 == 0){
                    sel("#all-components").innerHTML += "<div class='com-name'>"+result[i].replace(/\</g,"&lt;").replace(/\>/g,"&gt;")+":</div>";
                    sel("#all-components").innerHTML += "<div class='com-tem'>"+result[++i].replace(/\</g,"&lt;").replace(/\>/g,"&gt;")+"</div><br>";
                }
            }
        });
        chrome.devtools.inspectedWindow.eval("document.querySelector('html').innerHTML", function(result, isException){
        var newResult = result.replace(/\</g,"&lt;").replace(/\>/g,"&gt;").replace(/\n/g,"<br>").replace(/ /g,"&nbsp;");
        chrome.devtools.inspectedWindow.eval("Per().component.getAllComponent()", function(result, isException){
            for(var i=0;i<result.length;i++){
                if(i%2 == 0){
                    var reg = new RegExp("&lt;"+result[i]+"&gt;","g");
                    newResult = newResult.replace(reg,"<span style='color:red;'>&lt;"+result[i]+"&gt;</span><span style='color:#1E9FFF;'>");
                    var reg = new RegExp("&lt;/"+result[i]+"&gt;","g");
                    newResult = newResult.replace(reg,"</span><span style='color:red;'>&lt;/"+result[i]+"&gt;</span>");
                }
            }
            sel("#all-components-html code").innerHTML = newResult;
        });
        
    });
    }
    sel("#title-console").onclick = function(){
        sel("#real-window-console").style.display = "block";
        sel("#real-window-component").style.display = "none";
    }
    sel("#title-components").onclick = function(){
        sel("#real-window-console").style.display = "none";
        sel("#real-window-component").style.display = "block";
    }
    sel("#set-component-button").onclick = function(){
        var comName = sel("#set-component-name").value;
        var comTem = sel("#set-component-tem").value;
        var allComList;
        chrome.devtools.inspectedWindow.eval("Per().component.getAllComponent()", function(result, isException){
            allComList = result;
            var isHaveThisCom = false;
            for(var i=0;i<allComList.length;i++){
                if(i%2 == 0 && allComList[i] == comName){
                    isHaveThisCom = true;
                }
            }
            if(isHaveThisCom){
                chrome.devtools.inspectedWindow.eval("Per().component.remove('"+comName+"')", function(result, isException){
                    chrome.devtools.inspectedWindow.eval("Per().component.set('"+comName+"','"+comTem+"')", function(result, isException){});
                });
            }else{
                chrome.devtools.inspectedWindow.eval("Per().component.set('"+comName+"','"+comTem+"')", function(result, isException){});

            }
        });
    }
    chrome.devtools.inspectedWindow.eval("document.querySelector('html').innerHTML", function(result, isException){
        var newResult = result.replace(/\</g,"&lt;").replace(/\>/g,"&gt;").replace(/\n/g,"<br>").replace(/ /g,"&nbsp;");
        chrome.devtools.inspectedWindow.eval("Per().component.getAllComponent()", function(result, isException){
            for(var i=0;i<result.length;i++){
                if(i%2 == 0){
                    var reg = new RegExp("&lt;"+result[i]+"&gt;","g");
                    newResult = newResult.replace(reg,"<span style='color:red;'>&lt;"+result[i]+"&gt;</span><span style='color:#1E9FFF;'>");
                    var reg = new RegExp("&lt;/"+result[i]+"&gt;","g");
                    newResult = newResult.replace(reg,"</span><span style='color:red;'>&lt;/"+result[i]+"&gt;</span>");
                }
            }
            sel("#all-components-html code").innerHTML = newResult;
        });
    });
    document.onkeydown = function(){
        if(event.keyCode == 13&&sel("#code-enter-area") == document.activeElement){
            var code = sel("#code-enter-area").value;
            chrome.devtools.inspectedWindow.eval(code, function(result, isException){
                if(typeof result == "object"&&Array.isArray(result) == false){
                    sel("#code-condition-list").innerText += JSON.stringify(result);
                    sel("#code-condition-list").innerHTML += "<hr>";
                    sel("#code-enter-area").value = "";
                }else{
                    sel("#code-condition-list").innerText += result;
                    sel("#code-condition-list").innerHTML += "<hr>";
                    sel("#code-enter-area").value = "";
                }
            });
        }
    }
});